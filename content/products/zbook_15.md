{
    "title": "Hewlett Packard ZBook 15",
    "date": "2020-04-03T22:35:06+05:30",
    "tags": ["WorkSation", "zbook"],
    "categories": ["Portatil", "WorkStation"],
    "images": ["img/zbook15/frente.png", "img/zbook15/lateral1.png",
		"img/zbook15/lateral2.png", "img/zbook15/frente2.png"],
    "thumbnailImage": "img/zbook15/frente2.png",
    "actualPrice": "$1.430.000",
    "comparePrice": null,
    "inStock": true,
    "options": {
            "RAM": ["16GB"],
			"Procesador":["Intel ® Core ™ i7-4900MQ CPU @ 2.80GHz"],
			"CPU": ["4 Cores, 8 threads"],
			"Base": ["no"],
			"Bios": ["Privativa"],
            "Disco Duro" : ["500GB"],
			"Bateria": ["100%"],
			"Tarjeta Inalámbrica": ["Libre"],
			"S.O": ["Gnu/Linux"],
			"Pantalla": ["15'"],
			"Distribución": ["Parabola", "PureOS", "Trisquel 8"]
    }
}

Portatil de segunda mano HP de la línea Zbook con muy buena capacidad de procesamiento y buena opción para altos requerimientos de Procesamiento.
