{
    "title": "Lenovo ThinkPad X220",
    "date": "2019-09-03T22:35:06+05:o30",
    "tags": ["portatil", "coreboot", "thinkpad", "x220"],
    "categories": ["Portatil"],
    "images": ["img/x220/frente.jpg", "img/x220/lateral.jpg", "img/x220/lateral2.jpg", "img/x220/trasera.jpg"],
    "thumbnailImage": "img/x220/miniatura.jpg",
    "actualPrice": "$640.000",
    "comparePrice": null,
    "inStock": true,
    "options": {
            "RAM": ["4GB"],
			"Procesador":["Intel ® Core ™ i5-2520M CPU @ 2.50GHz"],
			"CPU": ["2"],
			"Base": ["no"],
			"Bios": ["Coreboot/SeaBios"],
            "Disco Duro" : ["320GB"],
			"Bateria": ["74%"],
			"S.O": ["Gnu/Linux"],
			"Distribución": ["Parabola", "PureOS", "Trisquel 8"]
    }
}

Portatil de segunda mano X220 de la línea ThinkPad. Se le realizó Migración de la bios a Coreboot (SeaBios).
